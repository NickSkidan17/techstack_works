CREATE TABLE IF NOT EXISTS users (
   id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
   login varchar(70) NOT NULL,
   password varchar(70) NOT NULL,
   email varchar(70) NOT NULL
);
CREATE TABLE IF NOT EXISTS roles (
   id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
   name varchar(45) NULL
);
CREATE TABLE IF NOT EXISTS user_roles (
   user_id int NOT NULL,
   role_id int NOT NULL,
   FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE,
   FOREIGN KEY (role_id) REFERENCES roles (id) ON DELETE CASCADE,
   PRIMARY KEY (user_id, role_id)
);
CREATE TABLE IF NOT EXISTS images (
   id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
   image_name varchar(45) NOT NULL,
   image_type varchar(20) NOT NULL,
   image_data mediumblob NOT NULL
);
CREATE TABLE IF NOT EXISTS videos (
   id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
   video_name varchar(45) NOT NULL,
   video_type varchar(20) NOT NULL,
   video_data longblob NOT NULL
);
CREATE TABLE IF NOT EXISTS text_files (
   id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
   text_file_name varchar(45) NOT NULL,
   text_file_type varchar(20) NOT NULL,
   text_file_data mediumblob NOT NULL
);
CREATE TABLE IF NOT EXISTS favorite_images (
   user_id int NOT NULL,
   image_id int NOT NULL,
   FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE,
   FOREIGN KEY (image_id) REFERENCES images (id) ON DELETE CASCADE,
   PRIMARY KEY (user_id, image_id)
);
CREATE TABLE IF NOT EXISTS favorite_videos (
   user_id int NOT NULL,
   video_id int NOT NULL,
   FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE,
   FOREIGN KEY (video_id) REFERENCES videos (id) ON DELETE CASCADE,
   PRIMARY KEY (user_id, video_id)
);
CREATE TABLE IF NOT EXISTS favorite_text_files (
   user_id int NOT NULL,
   text_file_id int NOT NULL,
   FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE,
   FOREIGN KEY (text_file_id) REFERENCES text_files (id) ON DELETE CASCADE,
   PRIMARY KEY (user_id, text_file_id)
);

INSERT INTO roles (id, name) VALUES ('1', 'ROLE_ADMIN');
INSERT INTO roles (id, name) VALUES ('2', 'ROLE_USER');

INSERT INTO users (id, login, password, email)
VALUES ('1', 'nickolas', '$2a$11$uSXS6rLJ91WjgOHhEGDx..VGs7MkKZV68Lv5r1uwFu7HgtRn3dcXG', 'nickolas@mail.ru');

INSERT INTO user_roles (user_id, role_id) VALUES ('1', '1');