<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>User Data</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="bg1">
    <div class="generic-container">
        <div class="panel panel-default">
              <!-- Default panel contents -->
            <div class="panel-heading"><span class="lead">Your Data </span></div>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Login</th>
                        <th>Email</th>
                        <th width="100"></th>
                        <th width="100"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>${user.login}</td>
                        <td>${user.email}</td>
                        <td><a href="<c:url value='/user' />" class="btn btn-success custom-width">edit</a></td>
                        </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-12"><span class="pull-right">
            <a href="<c:url value='/home_page' />">Back To Library</a>
        </span></div>
    </div>
</div>
</body>
</html>