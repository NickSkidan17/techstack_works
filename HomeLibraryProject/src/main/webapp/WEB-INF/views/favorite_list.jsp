<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html; utf-8" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Images</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">

    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

</head>
<body>
<div class="bg1">
<c:if test="${empty favoriteFiles}">
    <h3 class="text-center">You have not chosen your favorite files yet</h3>
</c:if>
<c:if test="${not empty favoriteFiles}">
    <div class="generic-container">
        <div class="panel panel-default">
              <!-- Default panel contents -->
            <div class="panel-heading"><span class="lead">My Favorite Files </span></div>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th> Name </th>
                        <th> Type </th>
                    </tr>
                </thead>
                <tbody>
                <c:forEach items="${favoriteFiles}" var="file">
                    <tr>
                        <c:choose>
                            <c:when test="${file['class'].simpleName eq 'Image'}">
                                <td> ${file.imageName} </td>
                                <td> ${file.imageType} </td>
                                <td>
                                    <form:form method="POST" action="${contextPath}/favorite_list/image/${file.id}">
                                        <input type="hidden" name="_method" value="delete">
                                        <button class="btn btn-danger custom-width" type="submit">delete from favorite</button>
                                    </form:form>
                                </td>
                            </c:when>
                            <c:when test="${file['class'].simpleName eq 'TextFile'}">
                                <td> ${file.textFileName} </td>
                                <td> ${file.textFileType} </td>
                                <td>
                                    <form:form method="POST" action="${contextPath}/favorite_list/text_file/${file.id}">
                                        <input type="hidden" name="_method" value="delete">
                                        <button class="btn btn-danger custom-width" type="submit">delete from favorite</button>
                                    </form:form>
                                </td>
                            </c:when>
                            <c:otherwise>
                                <td> ${file.videoName} </td>
                                <td> ${file.videoType} </td>
                                <td>
                                    <form:form method="POST" action="${contextPath}/favorite_list/video/${file.id}">
                                        <input type="hidden" name="_method" value="delete">
                                        <button class="btn btn-danger custom-width" type="submit">delete from favorite</button>
                                    </form:form>
                                </td>
                            </c:otherwise>
                        </c:choose>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</c:if>

       <c:if test="${imageDeletedRows == 1}">
           <h4 class="text-center">Image ${imageName} was deleted from favorite files</h4>
       </c:if>
       <h3 class="text-center">${imageError}</h3>
       <c:if test="${textFileDeletedRows == 1}">
           <h4 class="text-center">Text File ${textFileName} was deleted from favorite files</h4>
       </c:if>
       <h3 class="text-center">${textFileError}</h3>
       <c:if test="${videoDeletedRows == 1}">
           <h4 class="text-center">Video ${videoName} was deleted from favorite files</h4>
       </c:if>
       <h3 class="text-center">${videoError}</h3>

        <span style="float: right">
            <a href="<c:url value='/home_page' />">Back To Library</a>
        </span>
</div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="${contextPath}/resources/js/bootstrap.min.js"></script>

</body>
</html>