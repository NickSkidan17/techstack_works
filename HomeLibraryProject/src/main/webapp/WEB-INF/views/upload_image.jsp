<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Upload Image</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">


    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

</head>
<body>
<div class="bg3">
   <div class="container">
     <h3 class="text-center">Upload New Image</h3>
     <h4 class="text-center">${emptyFile}</h4>
     <h4 class="text-center">${wrongType}</h4>
     <h4 class="text-center">${nullFile}</h4>
     <form:form method="POST" action="${contextPath}/upload_image?${_csrf.parameterName}=${_csrf.token}" encType="multipart/form-data">
        Please select a file to upload : <br/>
       <input type="file" name="file"/>
       <br/>
       <button type="submit">Add Image</button> or <a href="<c:url value='/images' />" class="text-primary">Cancel</a>
     </form:form>
   </div>
</div>
</body>
</html>