<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>List of Users</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="bg1">
    <c:if test="${usersQuantity == 0}">
             <h3 class="text-center">${emptyList}</h3><br/>
        </c:if>
        <div class="generic-container">
        <c:if test="${usersQuantity > 0}">
        <div class="panel panel-default">
              <!-- Default panel contents -->
            <div class="panel-heading"><span class="lead">List of Users </span></div>
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th><a href="${contextPath}/admin?sortOrder=${reverseSortOrder}&usersQuantityOnPage=${usersQuantityOnPage}&page=${page}">Login</a></th>
                        <th>Email</th>
                        <th width="100"></th>
                        <th width="100"></th>
                    </tr>
                </thead>
                <tbody>
                <c:forEach items="${users}" var="user">
                    <tr>
                        <td>${user.login}</td>
                        <td>${user.email}</td>
                        <td>
                        <form:form method="POST" action="${contextPath}/admin/${user.id}">
                             <input type="hidden" name="_method" value="delete"/>
                             <button class="btn btn-danger custom-width" type="submit">delete</button>
                        </form:form>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>

        <h3 class="text-center">${emptyList}</h3><br/>
            <c:if test="${deletedRows == 1}">
                <h4 class="text-center">User ${login} was deleted</h4>
            </c:if>

    </br>

        <c:choose>
                <c:when test="${pagesQuantity > 1}">
                    <ul class="pagination">
                        <c:if test="${page > 1}">
                            <c:url value="/admin?sortOrder=${sortOrder}&usersQuantityOnPage=${usersQuantityOnPage}" var="prevUrl">
                                <c:param name="page" value="${page - 1}"/>
                            </c:url>
                            <li class="page-item"><a class="page-link" href="<c:out value="${prevUrl}"/>">Previous</a></li>
                        </c:if>
                        <c:set var="begin" value="1" />
                        <c:set var="end" value="9" />
                        <c:choose>
                            <c:when test="${page - 4 <= 1}">
                                <c:if test="${pagesQuantity < 9}">
                                    <c:set var="end" value="${pagesQuantity}" />
                                </c:if>
                            </c:when>
                            <c:otherwise>
                                <c:set var="begin" value="${page - 4}" />
                                <c:set var="end" value="${begin + 8}" />
                                <c:if test="${end >= pagesQuantity}">
                                    <c:set var="begin" value="1" />
                                    <c:set var="end" value="${pagesQuantity}" />
                                    <c:if test="${pagesQuantity > 9}">
                                        <c:set var="begin" value="${pagesQuantity - 8}" />
                                    </c:if>
                                </c:if>
                            </c:otherwise>
                        </c:choose>
                        <c:forEach var="i" begin="${begin}" end="${end}" step="1">
                            <c:url value="/admin?sortOrder=${sortOrder}&usersQuantityOnPage=${usersQuantityOnPage}" var="url">
                                <c:param name="page" value="${i}"/>
                            </c:url>
                            <c:choose>
                                <c:when test="${i == page}">
                                    <li class="page-item"><a class="page-link" href="<c:out value="${url}"/>" class="page" ><c:out value="${i}" /></a></li>
                                </c:when>
                                <c:otherwise>
                                    <li class="page-item"><a class="page-link" href="<c:out value="${url}"/>" ><c:out value="${i}" /></a></li>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                        <c:if test="${pagesQuantity > page}">
                            <c:url value="/admin?sortOrder=${sortOrder}&usersQuantityOnPage=${usersQuantityOnPage}" var="nextUrl">
                                <c:param name="page" value="${page + 1}"/>
                            </c:url>
                            <li class="page-item"><a class="page-link" href="<c:out value="${nextUrl}"/>">Next</a></li>
                        </c:if>
                    </ul>
                </c:when>
                <c:otherwise>
                    <div align="center">
                    </div>
                </c:otherwise>
            </c:choose>

    <p class="font-weight-light">Page ${page}</p>
<form:form  action="${contextPath}/admin?usersQuantityOnPage=${usersQuantityOnPage}" method="GET">
    <div class="container">
      <div style="width:400px; margin:0 auto;">
      <label for="usersQuantityOnPage">Users on Page:</label>
         <select id="usersQuantityOnPage" name="usersQuantityOnPage">
          <option value="5" ${usersQuantityOnPage == '5' ? 'selected' : ''}>5</option>
          <option value="10" ${usersQuantityOnPage == '10' ? 'selected' : ''}>10</option>
          <option value="15" ${usersQuantityOnPage == '15' ? 'selected' : ''}>15</option>
         </select>
      <button type="submit" class="btn btn-primary">Select</button>
      </div>
    </div>
</form:form>
<br/>
        </c:if>
        <div class="row">
        <div class="col-md-6">
            <a href="<c:url value='/admin/user' />">Add New User</a>
        </div>
        <div class="col-md-6"><span class="pull-right">
            <a href="<c:url value='/home_page' />">Back to Library</a>
        </span></div>
        </div>
        </div>
        <div class="container">
              <div style="width:400px; margin:0 auto;">
                Total number of Users : ${usersQuantity}
              </div>
        </div>
</div>
</body>
</html>