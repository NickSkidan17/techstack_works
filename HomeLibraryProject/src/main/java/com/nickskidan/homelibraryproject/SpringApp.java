package com.nickskidan.homelibraryproject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.nickskidan.homelibraryproject")
public class SpringApp extends SpringBootServletInitializer {

    private final static Logger LOGGER = LoggerFactory.getLogger(SpringApp.class);

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(SpringApp.class);
    }

    public static void main(String[] args) {
        LOGGER.info("RUN");
        SpringApplication.run(SpringApp.class, args);
    }
}