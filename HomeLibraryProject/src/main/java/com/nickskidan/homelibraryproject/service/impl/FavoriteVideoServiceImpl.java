package com.nickskidan.homelibraryproject.service.impl;

import com.nickskidan.homelibraryproject.dao.api.FavoriteVideoDao;
import com.nickskidan.homelibraryproject.model.FavoriteVideo;
import com.nickskidan.homelibraryproject.model.User;
import com.nickskidan.homelibraryproject.model.Video;
import com.nickskidan.homelibraryproject.service.api.FavoriteVideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class FavoriteVideoServiceImpl implements FavoriteVideoService {

    private FavoriteVideoDao favoriteVideoDao;

    @Autowired
    FavoriteVideoServiceImpl(FavoriteVideoDao favoriteVideoDao) {
        this.favoriteVideoDao = favoriteVideoDao;
    }

    @Override
    public void insertFavoriteVideo(User user, Video video) {
        favoriteVideoDao.insertFavoriteVideo(user, video);
    }

    @Override
    public ArrayList<Integer> findAllVideosIdByUser(User user) {
        return favoriteVideoDao.findAllVideosIdByUser(user);
    }

    @Override
    public int deleteFromFavoriteByVideoId(int id) {
        return favoriteVideoDao.deleteFromFavoriteByVideoId(id);
    }

    @Override
    public FavoriteVideo findByUserIdAndVideoId(User user, Video video) {
        return favoriteVideoDao.findByUserIdAndVideoId(user, video);
    }
}