package com.nickskidan.homelibraryproject.service.api;

import com.nickskidan.homelibraryproject.model.User;

public interface UserRolesService {

    void insertUserRoles(User user);
}