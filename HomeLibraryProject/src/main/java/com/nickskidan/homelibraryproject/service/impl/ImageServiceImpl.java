package com.nickskidan.homelibraryproject.service.impl;

import com.nickskidan.homelibraryproject.dao.api.ImageDao;
import com.nickskidan.homelibraryproject.model.Image;
import com.nickskidan.homelibraryproject.service.api.ImageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class ImageServiceImpl implements ImageService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ImageServiceImpl.class);

    private ImageDao imageDao;

    @Autowired
    ImageServiceImpl(ImageDao imageDao) {
        this.imageDao = imageDao;
    }

    @Override
    public Image findById(Integer id) {
        return imageDao.findById(id);
    }

    @Override
    public List<Image> getAllImagesSortedByNameWithPagination(String sortOrder, int imagesQuantityOnPage, int page) {
        return imageDao.getAllImagesSortedByNameWithPagination(sortOrder, imagesQuantityOnPage, page);
    }

    @Override
    public int deleteImageById(int id) {
        return imageDao.deleteImageById(id);
    }

    @Override
    public void saveImage(MultipartFile file) {
        if (file != null) {
            String imageName = file.getOriginalFilename();
            try {
                Image image = new Image(imageName, file.getContentType(), file.getBytes());
                imageDao.saveImage(image);
            } catch (IOException e) {
                LOGGER.error("Image wasn't saved");
            }
        } else {
            LOGGER.error("Trying to save null file");
        }
    }

    @Override
    public int countImages() {
        return imageDao.countImages();
    }

    @Override
    public List<Image> findAllFavoriteImages(ArrayList<Integer> imagesId) {
        return imageDao.findAllFavoriteImages(imagesId);
    }
}