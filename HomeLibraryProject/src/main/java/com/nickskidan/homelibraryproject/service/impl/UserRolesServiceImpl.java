package com.nickskidan.homelibraryproject.service.impl;

import com.nickskidan.homelibraryproject.dao.api.UserRolesDao;
import com.nickskidan.homelibraryproject.model.User;
import com.nickskidan.homelibraryproject.service.api.UserRolesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserRolesServiceImpl implements UserRolesService {

    private UserRolesDao userRolesDao;

    @Autowired
    UserRolesServiceImpl(UserRolesDao userRolesDao) {
        this.userRolesDao = userRolesDao;
    }

    @Override
    public void insertUserRoles(User user) {
        userRolesDao.insertUserRoles(user);
    }
}