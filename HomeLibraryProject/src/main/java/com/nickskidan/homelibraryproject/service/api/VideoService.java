package com.nickskidan.homelibraryproject.service.api;

import com.nickskidan.homelibraryproject.model.Video;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

public interface VideoService {

    Video findById(Integer id);

    List<Video> getAllVideosSortedByNameWithPagination(String sortOrder, int videosQuantityOnPage, int page);

    int deleteVideoById(int id);

    void saveVideo(MultipartFile video);

    int countVideos();

    List<Video> findAllFavoriteVideos(ArrayList<Integer> videosId);
}