package com.nickskidan.homelibraryproject.service.api;

import com.nickskidan.homelibraryproject.model.TextFile;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

public interface TextFileService {

    TextFile findById(Integer id);

    List<TextFile> getAllTextFilesSortedByNameWithPagination(String sortOrder, int textFilesQuantityOnPage, int page);

    int deleteTextFileById(int id);

    void saveTextFile(MultipartFile textFile);

    int countTextFiles();

    List<TextFile> findAllFavoriteTextFiles(ArrayList<Integer> textFilesId);
}