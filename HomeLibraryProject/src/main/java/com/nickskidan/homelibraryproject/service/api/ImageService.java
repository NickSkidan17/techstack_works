package com.nickskidan.homelibraryproject.service.api;

import com.nickskidan.homelibraryproject.model.Image;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

public interface ImageService {

    Image findById(Integer id);

    List<Image> getAllImagesSortedByNameWithPagination(String sortOrder, int imagesQuantityOnPage, int page);

    int deleteImageById(int id);

    void saveImage(MultipartFile image);

    int countImages();

    List<Image> findAllFavoriteImages(ArrayList<Integer> imagesId);
}