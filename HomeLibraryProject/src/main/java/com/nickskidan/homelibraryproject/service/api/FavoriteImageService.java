package com.nickskidan.homelibraryproject.service.api;

import com.nickskidan.homelibraryproject.model.FavoriteImage;
import com.nickskidan.homelibraryproject.model.Image;
import com.nickskidan.homelibraryproject.model.User;

import java.util.ArrayList;

public interface FavoriteImageService {

    void insertFavoriteImage(User user, Image image);

    ArrayList<Integer> findAllImagesIdByUser(User user);

    int deleteFromFavoriteByImageId(int id);

    FavoriteImage findByImageIdAndUserId(User user, Image image);
}