package com.nickskidan.homelibraryproject.service.impl;

import com.nickskidan.homelibraryproject.dao.api.RoleDao;
import com.nickskidan.homelibraryproject.dao.api.UserDao;
import com.nickskidan.homelibraryproject.dao.api.UserRolesDao;
import com.nickskidan.homelibraryproject.model.Role;
import com.nickskidan.homelibraryproject.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserDetailsServiceImpl.class);

    private UserDao userDao;
    private RoleDao roleDao;
    private UserRolesDao userRolesDao;

    @Autowired
    UserDetailsServiceImpl(UserDao userDao, RoleDao roleDao, UserRolesDao userRolesDao) {
        this.userDao = userDao;
        this.roleDao = roleDao;
        this.userRolesDao = userRolesDao;
    }

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        LOGGER.debug("Authentication {}", login);
        User userFindByLogin = userDao.findByLogin(login);
        User userFindByEmail = userDao.findByEmail(login);
        if (userFindByLogin != null) {
            return new org.springframework.security.core.userdetails.User(userFindByLogin.getLogin(), userFindByLogin.getPassword(), getGrantedAuthorities(userFindByLogin));
        } else if (userFindByEmail != null) {
            return new org.springframework.security.core.userdetails.User(userFindByEmail.getLogin(), userFindByEmail.getPassword(), getGrantedAuthorities(userFindByEmail));
        } else {
            LOGGER.info("User is not found");
            throw new UsernameNotFoundException("Unknown user");
        }
    }

    private Set<GrantedAuthority> getGrantedAuthorities(User user) {
        Set<GrantedAuthority> grantedAuthorities = new HashSet<GrantedAuthority>();
        Set<Role> roles = roleDao.getRolesByUserId(user.getId());
        while (roles == null) {
            userRolesDao.insertUserRoles(user);
            roles = roleDao.getRolesByUserId(user.getId());
        }
        LOGGER.info("User {} has role {}", user.getLogin(), roles);
        for (Role role : roles) {
            grantedAuthorities.add(new SimpleGrantedAuthority(role.getName()));
        }
        return grantedAuthorities;
    }
}