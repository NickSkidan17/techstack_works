package com.nickskidan.homelibraryproject.service.api;

import com.nickskidan.homelibraryproject.model.FavoriteVideo;
import com.nickskidan.homelibraryproject.model.User;
import com.nickskidan.homelibraryproject.model.Video;

import java.util.ArrayList;

public interface FavoriteVideoService {

    void insertFavoriteVideo(User user, Video video);

    ArrayList<Integer> findAllVideosIdByUser(User user);

    int deleteFromFavoriteByVideoId(int id);

    FavoriteVideo findByUserIdAndVideoId(User user, Video video);
}