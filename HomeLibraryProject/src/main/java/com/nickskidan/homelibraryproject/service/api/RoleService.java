package com.nickskidan.homelibraryproject.service.api;

import com.nickskidan.homelibraryproject.model.Role;

import java.util.Set;

public interface RoleService {

    Set<Role> getRolesByUserId(int id);
}