package com.nickskidan.homelibraryproject.service.impl;

import com.nickskidan.homelibraryproject.dao.api.UserDao;
import com.nickskidan.homelibraryproject.model.User;
import com.nickskidan.homelibraryproject.service.api.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

    private UserDao userDao;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    UserServiceImpl(UserDao userDao, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userDao = userDao;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public User findByLogin(String login) {
        return userDao.findByLogin(login);
    }

    @Override
    public User findByEmail(String email) {
        return userDao.findByEmail(email);
    }

    @Override
    public User findById(int id) {
        return userDao.findById(id);
    }

    @Override
    public void saveUser(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        LOGGER.debug("Saving user in db with an id {} ", user.getId());
        userDao.insertUser(user);
    }

    @Override
    public List<User> getAllUsersSortedByLoginWithPagination(String sortOrder, int usersQuantityOnPage, int page) {
        return userDao.getAllUsersSortedByLoginWithPagination(sortOrder, usersQuantityOnPage, page);
    }

    @Override
    public int deleteUserById(int id) {
        return userDao.deleteUserById(id);
    }

    @Override
    public User updateUser(User user) {
        Optional<User> updatedUser = Optional.ofNullable(userDao.findById(user.getId()));
        if (updatedUser.isPresent()) {
            User userForUpdating = updatedUser.get();
            LOGGER.info("User with login {} will be updated", userForUpdating.getLogin());
            userForUpdating.setLogin(user.getLogin());
            userForUpdating.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            userForUpdating.setEmail(user.getEmail());
            User userAfterUpdating = userDao.updateUser(userForUpdating);
            if (!user.equals(userAfterUpdating)) {
                LOGGER.info("User {} was successfully updated", userForUpdating.getLogin());
            }
            return userAfterUpdating;
        } else {
            LOGGER.error("No user for updating");
            return null;
        }
    }

    @Override
    public int countUsers() {
        return userDao.countUsers();
    }
}