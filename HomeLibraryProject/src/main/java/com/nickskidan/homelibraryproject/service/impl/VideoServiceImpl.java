package com.nickskidan.homelibraryproject.service.impl;

import com.nickskidan.homelibraryproject.dao.api.VideoDao;
import com.nickskidan.homelibraryproject.model.Video;
import com.nickskidan.homelibraryproject.service.api.VideoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class VideoServiceImpl implements VideoService {

    private static final Logger LOGGER = LoggerFactory.getLogger(VideoServiceImpl.class);

    private VideoDao videoDao;

    @Autowired
    VideoServiceImpl(VideoDao videoDao) {
        this.videoDao = videoDao;
    }

    @Override
    public Video findById(Integer id) {
        return videoDao.findById(id);
    }

    @Override
    public List<Video> getAllVideosSortedByNameWithPagination(String sortOrder, int videosQuantityOnPage, int page) {
        return videoDao.getAllVideosSortedByNameWithPagination(sortOrder, videosQuantityOnPage, page);
    }

    @Override
    public int deleteVideoById(int id) {
        return videoDao.deleteVideoById(id);
    }

    @Override
    public void saveVideo(MultipartFile file) {
        if (file != null) {
            String videoName = file.getOriginalFilename();
            try {
                Video video = new Video(videoName, file.getContentType(), file.getBytes());
                videoDao.saveVideo(video);
            } catch (IOException e) {
                LOGGER.error("Video wasn't saved");
            }
        } else {
            LOGGER.error("Trying to save null file");
        }
    }

    @Override
    public int countVideos() {
        return videoDao.countVideos();
    }

    @Override
    public List<Video> findAllFavoriteVideos(ArrayList<Integer> videosId) {
        return videoDao.findAllFavoriteVideos(videosId);
    }
}