package com.nickskidan.homelibraryproject.service.api;

import com.nickskidan.homelibraryproject.model.FavoriteTextFile;
import com.nickskidan.homelibraryproject.model.TextFile;
import com.nickskidan.homelibraryproject.model.User;

import java.util.ArrayList;

public interface FavoriteTextFileService {

    void insertFavoriteTextFile(User user, TextFile textFile);

    ArrayList<Integer> findAllTextFilesIdByUser(User user);

    int deleteFromFavoriteByTextFileId(int id);

    FavoriteTextFile findByUserIdAndTextFileId(User user, TextFile textFile);
}
