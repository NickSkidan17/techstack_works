package com.nickskidan.homelibraryproject.service.impl;

import com.nickskidan.homelibraryproject.dao.api.TextFileDao;
import com.nickskidan.homelibraryproject.model.TextFile;
import com.nickskidan.homelibraryproject.service.api.TextFileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class TextFileServiceImpl implements TextFileService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TextFileServiceImpl.class);

    private TextFileDao textFileDao;

    @Autowired
    TextFileServiceImpl(TextFileDao textFileDao) {
        this.textFileDao = textFileDao;
    }

    @Override
    public TextFile findById(Integer id) {
        return textFileDao.findById(id);
    }

    @Override
    public List<TextFile> getAllTextFilesSortedByNameWithPagination(String sortOrder, int textFilesQuantityOnPage, int page) {
        return textFileDao.getAllTextFilesSortedByNameWithPagination(sortOrder, textFilesQuantityOnPage, page);
    }

    @Override
    public int deleteTextFileById(int id) {
        return textFileDao.deleteTextFileById(id);
    }

    @Override
    public void saveTextFile(MultipartFile file) {
        if (file != null) {
            String textFileName = file.getOriginalFilename();
            try {
                TextFile textFile = new TextFile(textFileName, file.getContentType(), file.getBytes());
                textFileDao.saveTextFile(textFile);
            } catch (IOException e) {
                LOGGER.error("TextFile wasn't saved");
            }
        } else {
            LOGGER.error("Trying to save null file");
        }
    }

    @Override
    public int countTextFiles() {
        return textFileDao.countTextFiles();
    }

    @Override
    public List<TextFile> findAllFavoriteTextFiles(ArrayList<Integer> textFilesId) {
        return textFileDao.findAllFavoriteTextFiles(textFilesId);
    }
}