package com.nickskidan.homelibraryproject.service.api;

import com.nickskidan.homelibraryproject.model.User;

public interface SecurityService {

    User findLoggedInUser();

    void autoLogin(String login, String password);
}