package com.nickskidan.homelibraryproject.service.api;

import com.nickskidan.homelibraryproject.model.User;

import java.util.List;

public interface UserService {

    User findByLogin(String login);

    User findByEmail(String email);

    User findById(int id);

    void saveUser(User user);

    List<User> getAllUsersSortedByLoginWithPagination(String sortOrder, int usersQuantityOnPage, int page);

    int deleteUserById(int id);

    User updateUser(User user);

    int countUsers();
}