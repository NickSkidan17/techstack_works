package com.nickskidan.homelibraryproject.service.impl;

import com.nickskidan.homelibraryproject.dao.api.FavoriteTextFileDao;
import com.nickskidan.homelibraryproject.model.FavoriteTextFile;
import com.nickskidan.homelibraryproject.model.TextFile;
import com.nickskidan.homelibraryproject.model.User;
import com.nickskidan.homelibraryproject.service.api.FavoriteTextFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class FavoriteTextFileServiceImpl implements FavoriteTextFileService {

    private FavoriteTextFileDao favoriteTextFileDao;

    @Autowired
    FavoriteTextFileServiceImpl(FavoriteTextFileDao favoriteTextFileDao) {
        this.favoriteTextFileDao = favoriteTextFileDao;
    }

    @Override
    public void insertFavoriteTextFile(User user, TextFile textFile) {
        favoriteTextFileDao.insertFavoriteTextFile(user, textFile);
    }

    @Override
    public ArrayList<Integer> findAllTextFilesIdByUser(User user) {
        return favoriteTextFileDao.findAllTextFilesIdByUser(user);
    }

    @Override
    public int deleteFromFavoriteByTextFileId(int id) {
        return favoriteTextFileDao.deleteFromFavoriteByTextFileId(id);
    }

    @Override
    public FavoriteTextFile findByUserIdAndTextFileId(User user, TextFile textFile) {
        return favoriteTextFileDao.findByUserIdAndTextFileId(user, textFile);
    }
}