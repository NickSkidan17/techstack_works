package com.nickskidan.homelibraryproject.service.impl;

import com.nickskidan.homelibraryproject.dao.api.FavoriteImageDao;
import com.nickskidan.homelibraryproject.model.FavoriteImage;
import com.nickskidan.homelibraryproject.model.Image;
import com.nickskidan.homelibraryproject.model.User;
import com.nickskidan.homelibraryproject.service.api.FavoriteImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class FavoriteImageServiceImpl implements FavoriteImageService {

    private FavoriteImageDao favoriteImageDao;

    @Autowired
    FavoriteImageServiceImpl(FavoriteImageDao favoriteImageDao) {
        this.favoriteImageDao = favoriteImageDao;
    }

    @Override
    public void insertFavoriteImage(User user, Image image) {
        favoriteImageDao.insertFavoriteImage(user, image);
    }

    @Override
    public ArrayList<Integer> findAllImagesIdByUser(User user) {
        return favoriteImageDao.findAllImagesIdByUser(user);
    }

    @Override
    public int deleteFromFavoriteByImageId(int id) {
        return favoriteImageDao.deleteFromFavoriteByImageId(id);
    }

    @Override
    public FavoriteImage findByImageIdAndUserId(User user, Image image) {
        return favoriteImageDao.findByUserIdAndImageId(user, image);
    }
}