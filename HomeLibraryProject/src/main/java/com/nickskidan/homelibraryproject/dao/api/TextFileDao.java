package com.nickskidan.homelibraryproject.dao.api;

import com.nickskidan.homelibraryproject.model.TextFile;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public interface TextFileDao {
    RowMapper<TextFile> TEXT_FILE_ROW_MAPPER = (ResultSet rs, int rowNum) ->
            new TextFile(rs.getInt("id"), rs.getString("text_file_name"),
                    rs.getString("text_file_type"), rs.getBytes("text_file_data"));

    TextFile findById(Integer id);

    List<TextFile> getAllTextFilesSortedByNameWithPagination(String sortOrder, int textFilesQuantityOnPage, int page);

    int deleteTextFileById(int id);

    void saveTextFile(TextFile textFile);

    int countTextFiles();

    List<TextFile> findAllFavoriteTextFiles(ArrayList<Integer> textFilesId);
}