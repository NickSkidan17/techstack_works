package com.nickskidan.homelibraryproject.dao.api;

import com.nickskidan.homelibraryproject.model.Image;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public interface ImageDao {
    RowMapper<Image> IMAGE_ROW_MAPPER = (ResultSet rs, int rowNum) ->
            new Image(rs.getInt("id"), rs.getString("image_name"),
                    rs.getString("image_type"), rs.getBytes("image_data"));

    Image findById(Integer id);

    List<Image> getAllImagesSortedByNameWithPagination(String sortOrder, int imagesQuantityOnPage, int page);

    int deleteImageById(int id);

    void saveImage(Image image);

    int countImages();

    List<Image> findAllFavoriteImages(ArrayList<Integer> imagesId);
}