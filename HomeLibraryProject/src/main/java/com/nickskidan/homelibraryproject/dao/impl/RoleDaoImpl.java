package com.nickskidan.homelibraryproject.dao.impl;

import com.nickskidan.homelibraryproject.dao.api.RoleDao;
import com.nickskidan.homelibraryproject.model.Role;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.HashSet;
import java.util.Set;

@Repository(value = "roleDao")
public class RoleDaoImpl implements RoleDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(RoleDaoImpl.class);

    private JdbcTemplate jdbcTemplate;

    @Autowired
    RoleDaoImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public Set<Role> getRolesByUserId(int id) {
        LOGGER.debug("Finding roles for user with id {}", id);
        Set<Role> roles = new HashSet<>();
        Role role = null;
        try {
            role = jdbcTemplate.queryForObject("select r.* from roles r join user_roles ur on r.id = ur.role_id where ur.user_id = ?", new Object[]{id}, ROLE_ROW_MAPPER);
        } catch (DataAccessException dataAccessException) {
            LOGGER.error("Couldn't find roles for user with id {}", id);
        }
        if (role != null) {
            roles.add(role);
            return roles;
        } else return null;
    }
}