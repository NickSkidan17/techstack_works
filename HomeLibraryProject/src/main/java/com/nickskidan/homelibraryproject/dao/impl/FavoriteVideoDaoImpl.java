package com.nickskidan.homelibraryproject.dao.impl;

import com.nickskidan.homelibraryproject.dao.api.FavoriteVideoDao;
import com.nickskidan.homelibraryproject.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Repository(value = "favoriteVideoDao")
public class FavoriteVideoDaoImpl implements FavoriteVideoDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(FavoriteVideoDaoImpl.class);

    private JdbcTemplate jdbcTemplate;

    @Autowired
    FavoriteVideoDaoImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public void insertFavoriteVideo(User user, Video video) {
        jdbcTemplate.update("insert into favorite_videos values(?, ?)", user.getId(), video.getId());
        LOGGER.info("New data was inserted into favorite_videos");
    }

    @Override
    public ArrayList<Integer> findAllVideosIdByUser(User user) {
        LOGGER.debug("Finding all favorite videos id's of User {} ", user.getLogin());
        ArrayList<Integer> videosIdList = new ArrayList<>();
        String sql = "select * from favorite_videos where user_id = " + user.getId();
        try {
            List<FavoriteVideo> favoriteVideos = jdbcTemplate.query(sql, FAVORITE_VIDEO_ROW_MAPPER);
            for (FavoriteVideo favoriteVideo : favoriteVideos) {
                videosIdList.add(favoriteVideo.getVideoId());
            }
            LOGGER.info("Id's of user's favorite videos are {} ", videosIdList);
        } catch (DataAccessException dataAccessException) {
            LOGGER.error("Empty result");
        }
        return videosIdList.size() > 0 ? videosIdList : null;
    }

    @Override
    public int deleteFromFavoriteByVideoId(int id) {
        int rows = jdbcTemplate.update("delete from favorite_videos where video_id = ?", id);
        if (rows == 1) {
            LOGGER.info("Video with id {} was deleted from favorite", id);
        } else {
            LOGGER.error("Something went wrong during deleting");
        }
        return rows;
    }

    @Override
    public FavoriteVideo findByUserIdAndVideoId(User user, Video video) {
        LOGGER.debug("Finding favoriteVideo, where user_id {} and video_id {}", user.getId(), video.getId());
        FavoriteVideo favoriteVideo = null;
        try {
            favoriteVideo = jdbcTemplate.queryForObject("select * from favorite_videos where user_id = ? and video_id = ?", new Object[]{user.getId(), video.getId()}, FAVORITE_VIDEO_ROW_MAPPER);
        } catch (DataAccessException dataAccessException) {
            LOGGER.error("Couldn't find favoriteVideo");
        }
        return favoriteVideo;
    }
}