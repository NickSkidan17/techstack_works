package com.nickskidan.homelibraryproject.dao.impl;

import com.nickskidan.homelibraryproject.dao.api.UserDao;
import com.nickskidan.homelibraryproject.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Repository(value = "userDao")
public class UserDaoImpl implements UserDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserDaoImpl.class);

    private JdbcTemplate jdbcTemplate;

    @Autowired
    UserDaoImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public User findByLogin(String login) {
        LOGGER.debug("Finding User with login {}", login);
        User user = null;
        try {
            user = jdbcTemplate.queryForObject("select id, login, password, email from users where login = ?", new Object[]{login}, USER_ROW_MAPPER);
        } catch (DataAccessException dataAccessException) {
            LOGGER.error("Couldn't find User with login {}", login);
        }
        return user;
    }

    @Override
    public User findByEmail(String email) {
        LOGGER.debug("Finding User with email {}", email);
        User user = null;
        try {
            user = jdbcTemplate.queryForObject("select id, login, password, email from users where email = ?", new Object[]{email}, USER_ROW_MAPPER);
        } catch (DataAccessException dataAccessException) {
            LOGGER.error("Couldn't find User with email {}", email);
        }
        return user;
    }

    @Override
    public User findById(int id) {
        LOGGER.debug("Finding User with id {}", id);
        User user = null;
        try {
            user = jdbcTemplate.queryForObject("select id, login, password, email from users where id = ?", new Object[]{id}, USER_ROW_MAPPER);
        } catch (DataAccessException dataAccessException) {
            LOGGER.error("Couldn't find User with id {}", id);
        }
        return user;
    }


    @Override
    public void insertUser(User user) {
        jdbcTemplate.update("insert into users values (?, ?, ?, ?)", user.getId(), user.getLogin(), user.getPassword(), user.getEmail());
        LOGGER.info("New User {} was inserted", user);
    }

    @Override
    public List<User> getAllUsersSortedByLoginWithPagination(String sortOrder, int usersQuantityOnPage, int page) {
        LOGGER.debug("Finding all registered users");
        List<User> users = new ArrayList<>();
        String sql = "select * from users order by login "
                + sortOrder + " limit " + usersQuantityOnPage + " offset " + ((page - 1) * usersQuantityOnPage);
        try {
            users = jdbcTemplate.query(sql, USER_ROW_MAPPER);
        } catch (DataAccessException dataAccessException) {
            LOGGER.error("Empty result");
        }
        return users;
    }

    @Override
    public int deleteUserById(int id) {
        int rows = jdbcTemplate.update("delete from users where id = ?", id);
        if (rows == 1) {
            LOGGER.info("User with id {} was deleted", id);
        } else {
            LOGGER.error("Something went wrong during deleting");
        }
        return rows;
    }

    @Override
    public User updateUser(User user) {
        int rows = jdbcTemplate.update("update users set login = ?, password = ?, email = ? where (id = ?)", user.getLogin(), user.getPassword(), user.getEmail(), user.getId());
        if (rows == 1) {
            LOGGER.info("User with id {} was updated", user.getId());
            return findById(user.getId());
        } else {
            LOGGER.error("Something went wrong during updating");
            return null;
        }
    }

    @Override
    public int countUsers() {
        return jdbcTemplate.queryForObject("select count(*) from users", Integer.class);
    }
}