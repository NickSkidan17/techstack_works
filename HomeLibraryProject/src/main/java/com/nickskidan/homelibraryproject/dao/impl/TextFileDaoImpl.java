package com.nickskidan.homelibraryproject.dao.impl;

import com.nickskidan.homelibraryproject.dao.api.TextFileDao;
import com.nickskidan.homelibraryproject.model.TextFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Repository(value = "textFileDao")
public class TextFileDaoImpl implements TextFileDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(TextFileDaoImpl.class);

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    TextFileDaoImpl(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public TextFile findById(Integer id) {
        TextFile textFile = null;
        LOGGER.debug("Finding TextFile with id {}", id);
        try {
            textFile = namedParameterJdbcTemplate.queryForObject("select id, text_file_name, text_file_type, text_file_data from text_files where id = :id", new MapSqlParameterSource("id", id), TEXT_FILE_ROW_MAPPER);
        } catch (DataAccessException dataAccessException) {
            LOGGER.error("Couldn't find TextFile with id {}", id);
        }
        return textFile;
    }

    @Override
    public List<TextFile> getAllTextFilesSortedByNameWithPagination(String sortOrder, int textFilesQuantityOnPage, int page) {
        LOGGER.debug("Finding all textFiles from db order by name");
        List<TextFile> textFiles = new ArrayList<>();
        String sql = "select id, text_file_name, text_file_type, text_file_data from text_files order by text_file_name "
                + sortOrder + " limit " + textFilesQuantityOnPage + " offset " + ((page - 1) * textFilesQuantityOnPage);
        try {
            textFiles = namedParameterJdbcTemplate.query(sql, TEXT_FILE_ROW_MAPPER);
        } catch (DataAccessException dataAccessException) {
            LOGGER.error("Empty result");
        }
        return textFiles;
    }

    @Override
    public int deleteTextFileById(int id) {
        int rows = namedParameterJdbcTemplate.update("delete from text_files where id = :id", new MapSqlParameterSource("id", id));
        if (rows == 1) {
            LOGGER.info("TextFile with id {} was deleted", id);
        } else {
            LOGGER.error("Something went wrong during deleting");
        }
        return rows;
    }

    @Override
    public void saveTextFile(TextFile textFile) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("textFileName", textFile.getTextFileName())
                .addValue("textFileType", textFile.getTextFileType())
                .addValue("textFileData", textFile.getTextFileData());
        namedParameterJdbcTemplate.update("insert into text_files (text_file_name, text_file_type, text_file_data) values (:textFileName, :textFileType, :textFileData)", params);
        LOGGER.info("New TextFile {} was inserted", textFile);
    }

    @Override
    public int countTextFiles() {
        return namedParameterJdbcTemplate.queryForObject("select count(*) from text_files", (HashMap) null, Integer.class);
    }

    @Override
    public List<TextFile> findAllFavoriteTextFiles(ArrayList<Integer> textFilesId) {
        LOGGER.debug("Finding all favorite textFiles");
        List<TextFile> favoriteTextFiles = new ArrayList<>();
        if (textFilesId != null) {
            String sqlQueryForFavoriteTextFiles = "select * from text_files where id in (:textFilesId)";
            SqlParameterSource namedParameters = new MapSqlParameterSource("textFilesId", textFilesId);
            try {
                favoriteTextFiles = namedParameterJdbcTemplate.query(sqlQueryForFavoriteTextFiles, namedParameters, TEXT_FILE_ROW_MAPPER);
            } catch (DataAccessException dataAccessException) {
                LOGGER.error("Empty result");
            }
        }
        return favoriteTextFiles;
    }
}