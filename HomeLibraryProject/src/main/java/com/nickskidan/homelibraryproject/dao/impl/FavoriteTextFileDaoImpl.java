package com.nickskidan.homelibraryproject.dao.impl;

import com.nickskidan.homelibraryproject.dao.api.FavoriteTextFileDao;
import com.nickskidan.homelibraryproject.model.FavoriteTextFile;
import com.nickskidan.homelibraryproject.model.TextFile;
import com.nickskidan.homelibraryproject.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Repository(value = "favoriteTextFileDao")
public class FavoriteTextFileDaoImpl implements FavoriteTextFileDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(FavoriteTextFileDaoImpl.class);

    private JdbcTemplate jdbcTemplate;

    @Autowired
    FavoriteTextFileDaoImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public void insertFavoriteTextFile(User user, TextFile textFile) {
        jdbcTemplate.update("insert into favorite_text_files values(?, ?)", user.getId(), textFile.getId());
        LOGGER.info("New data was inserted into favorite_text_files");
    }

    @Override
    public ArrayList<Integer> findAllTextFilesIdByUser(User user) {
        LOGGER.debug("Finding all favorite textFiles id's of User {} ", user.getLogin());
        ArrayList<Integer> textFilesIdList = new ArrayList<>();
        String sql = "select * from favorite_text_files where user_id = " + user.getId();
        try {
            List<FavoriteTextFile> favoriteTextFiles = jdbcTemplate.query(sql, FAVORITE_TEXT_FILE_ROW_MAPPER);
            for (FavoriteTextFile favoriteTextFile : favoriteTextFiles) {
                textFilesIdList.add(favoriteTextFile.getTextFileId());
            }
            LOGGER.info("Id's of user's favorite textFiles are {} ", textFilesIdList);
        } catch (DataAccessException dataAccessException) {
            LOGGER.error("Empty result");
        }
        return textFilesIdList.size() > 0 ? textFilesIdList : null;
    }

    @Override
    public int deleteFromFavoriteByTextFileId(int id) {
        int rows = jdbcTemplate.update("delete from favorite_text_files where text_file_id = ?", id);
        if (rows == 1) {
            LOGGER.info("TextFile with id {} was deleted from favorite", id);
        } else {
            LOGGER.error("Something went wrong during deleting");
        }
        return rows;
    }

    @Override
    public FavoriteTextFile findByUserIdAndTextFileId(User user, TextFile textFile) {
        LOGGER.debug("Finding favoriteTextFile, where user_id {} and text_file_id {}", user.getId(), textFile.getId());
        FavoriteTextFile favoriteTextFile = null;
        try {
            favoriteTextFile = jdbcTemplate.queryForObject("select * from favorite_text_files where user_id = ? and text_file_id = ?", new Object[]{user.getId(), textFile.getId()}, FAVORITE_TEXT_FILE_ROW_MAPPER);
        } catch (DataAccessException dataAccessException) {
            LOGGER.error("Couldn't find favoriteTextFile");
        }
        return favoriteTextFile;
    }
}