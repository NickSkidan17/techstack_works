package com.nickskidan.homelibraryproject.dao.api;

import com.nickskidan.homelibraryproject.model.*;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.util.ArrayList;

public interface FavoriteVideoDao {
    RowMapper<FavoriteVideo> FAVORITE_VIDEO_ROW_MAPPER = (ResultSet rs, int rowNum) ->
            new FavoriteVideo(rs.getInt("user_id"), rs.getInt("video_id"));

    void insertFavoriteVideo(User user, Video video);

    ArrayList<Integer> findAllVideosIdByUser(User user);

    int deleteFromFavoriteByVideoId(int id);

    FavoriteVideo findByUserIdAndVideoId(User user, Video video);
}