package com.nickskidan.homelibraryproject.dao.api;

import com.nickskidan.homelibraryproject.model.Video;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public interface VideoDao {
    RowMapper<Video> VIDEO_ROW_MAPPER = (ResultSet rs, int rowNum) ->
            new Video(rs.getInt("id"), rs.getString("video_name"),
                    rs.getString("video_type"), rs.getBytes("video_data"));

    Video findById(Integer id);

    List<Video> getAllVideosSortedByNameWithPagination(String sortOrder, int videosQuantityOnPage, int page);

    int deleteVideoById(int id);

    void saveVideo(Video video);

    int countVideos();

    List<Video> findAllFavoriteVideos(ArrayList<Integer> videosId);
}