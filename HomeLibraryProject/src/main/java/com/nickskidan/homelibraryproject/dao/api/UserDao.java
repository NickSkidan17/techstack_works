package com.nickskidan.homelibraryproject.dao.api;

import com.nickskidan.homelibraryproject.model.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.util.List;

public interface UserDao {
    RowMapper<User> USER_ROW_MAPPER = (ResultSet rs, int rowNum) -> {
        User user = new User();
        user.setId(rs.getInt("id"));
        user.setLogin(rs.getString("login"));
        user.setPassword(rs.getString("password"));
        user.setEmail(rs.getString("email"));
        return user;
    };

    User findByLogin(String login);

    User findByEmail(String email);

    User findById(int id);

    void insertUser(User user);

    List<User> getAllUsersSortedByLoginWithPagination(String sortOrder, int usersQuantityOnPage, int page);

    int deleteUserById(int id);

    User updateUser(User user);

    int countUsers();
}