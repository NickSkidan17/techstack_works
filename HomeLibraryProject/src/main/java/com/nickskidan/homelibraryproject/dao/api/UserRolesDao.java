package com.nickskidan.homelibraryproject.dao.api;

import com.nickskidan.homelibraryproject.model.User;
import com.nickskidan.homelibraryproject.model.UserRoles;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;

public interface UserRolesDao {
    RowMapper<UserRoles> USER_ROLES_ROW_MAPPER = (ResultSet rs, int rowNum) ->
            new UserRoles(rs.getInt("user_id"), rs.getInt("role_id"));

    void insertUserRoles(User user);
}