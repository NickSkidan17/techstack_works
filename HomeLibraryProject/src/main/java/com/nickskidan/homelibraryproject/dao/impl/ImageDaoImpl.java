package com.nickskidan.homelibraryproject.dao.impl;

import com.nickskidan.homelibraryproject.dao.api.ImageDao;
import com.nickskidan.homelibraryproject.model.Image;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Repository(value = "imageDao")
public class ImageDaoImpl implements ImageDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(ImageDaoImpl.class);

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    ImageDaoImpl(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public Image findById(Integer id) {
        Image image = null;
        LOGGER.debug("Finding Image with id {}", id);
        try {
            image = namedParameterJdbcTemplate.queryForObject("select id, image_name, image_type, image_data from images where id = :id", new MapSqlParameterSource("id", id), IMAGE_ROW_MAPPER);
        } catch (DataAccessException dataAccessException) {
            LOGGER.error("Couldn't find Image with id {}", id);
        }
        return image;
    }

    @Override
    public List<Image> getAllImagesSortedByNameWithPagination(String sortOrder, int imagesQuantityOnPage, int page) {
        LOGGER.debug("Finding all images from db order by name");
        List<Image> images = new ArrayList<>();
        String sql = "select id, image_name, image_type, image_data from images order by image_name "
                + sortOrder + " limit " + imagesQuantityOnPage + " offset " + ((page - 1) * imagesQuantityOnPage);
        try {
            images = namedParameterJdbcTemplate.query(sql, IMAGE_ROW_MAPPER);
        } catch (DataAccessException dataAccessException) {
            LOGGER.error("Empty result");
        }
        return images;
    }

    @Override
    public int deleteImageById(int id) {
        int rows = namedParameterJdbcTemplate.update("delete from images where id = :id", new MapSqlParameterSource("id", id));
        if (rows == 1) {
            LOGGER.info("Image with id {} was deleted", id);
        } else {
            LOGGER.error("Something went wrong during deleting");
        }
        return rows;
    }

    @Override
    public void saveImage(Image image) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("imageName", image.getImageName())
                .addValue("imageType", image.getImageType())
                .addValue("imageData", image.getImageData());
        namedParameterJdbcTemplate.update("insert into images (image_name, image_type, image_data) values (:imageName, :imageType, :imageData)", params);
        LOGGER.info("New Image {} was inserted", image);
    }

    @Override
    public int countImages() {
        return namedParameterJdbcTemplate.queryForObject("select count(*) from images", (HashMap) null, Integer.class);
    }

    @Override
    public List<Image> findAllFavoriteImages(ArrayList<Integer> imagesId) {
        LOGGER.debug("Finding all favorite images");
        List<Image> favoriteImages = new ArrayList<>();
        if (imagesId != null) {
            String sqlQueryForFavoriteImages = "select * from images where id in (:imagesId)";
            SqlParameterSource namedParameters = new MapSqlParameterSource("imagesId", imagesId);
            try {
                favoriteImages = namedParameterJdbcTemplate.query(sqlQueryForFavoriteImages, namedParameters, IMAGE_ROW_MAPPER);
            } catch (DataAccessException dataAccessException) {
                LOGGER.error("Empty result");
            }
        }
        return favoriteImages;
    }
}