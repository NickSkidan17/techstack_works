package com.nickskidan.homelibraryproject.dao.api;

import com.nickskidan.homelibraryproject.model.Role;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.util.Set;

public interface RoleDao {
    RowMapper<Role> ROLE_ROW_MAPPER = (ResultSet rs, int rowNum) ->
            new Role(rs.getInt("id"), rs.getString("name"));

    Set<Role> getRolesByUserId(int id);
}