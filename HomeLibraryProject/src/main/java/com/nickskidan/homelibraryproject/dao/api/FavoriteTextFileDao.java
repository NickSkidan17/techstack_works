package com.nickskidan.homelibraryproject.dao.api;

import com.nickskidan.homelibraryproject.model.FavoriteTextFile;
import com.nickskidan.homelibraryproject.model.TextFile;
import com.nickskidan.homelibraryproject.model.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.util.ArrayList;

public interface FavoriteTextFileDao {
    RowMapper<FavoriteTextFile> FAVORITE_TEXT_FILE_ROW_MAPPER = (ResultSet rs, int rowNum) ->
            new FavoriteTextFile(rs.getInt("user_id"), rs.getInt("text_file_id"));

    void insertFavoriteTextFile(User user, TextFile textFile);

    ArrayList<Integer> findAllTextFilesIdByUser(User user);

    int deleteFromFavoriteByTextFileId(int id);

    FavoriteTextFile findByUserIdAndTextFileId(User user, TextFile textFile);
}