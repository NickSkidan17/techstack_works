package com.nickskidan.homelibraryproject.dao.impl;

import com.nickskidan.homelibraryproject.dao.api.VideoDao;
import com.nickskidan.homelibraryproject.model.Video;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Repository(value = "videoDao")
public class VideoDaoImpl implements VideoDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(VideoDaoImpl.class);

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    VideoDaoImpl(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public Video findById(Integer id) {
        Video video = null;
        LOGGER.debug("Finding Video with id {}", id);
        try {
            video = namedParameterJdbcTemplate.queryForObject("select id, video_name, video_type, video_data from videos where id = :id", new MapSqlParameterSource("id", id), VIDEO_ROW_MAPPER);
        } catch (DataAccessException dataAccessException) {
            LOGGER.error("Couldn't find Video with id {}", id);
        }
        return video;
    }

    @Override
    public List<Video> getAllVideosSortedByNameWithPagination(String sortOrder, int videosQuantityOnPage, int page) {
        LOGGER.debug("Finding all videos from db");
        List<Video> videos = new ArrayList<>();
        String sql = "select id, video_name, video_type, video_data from videos order by video_name "
                + sortOrder + " limit " + videosQuantityOnPage + " offset " + ((page - 1) * videosQuantityOnPage);
        try {
            videos = namedParameterJdbcTemplate.query(sql, VIDEO_ROW_MAPPER);
        } catch (DataAccessException dataAccessException) {
            LOGGER.error("Empty result");
        }
        return videos;
    }

    @Override
    public int deleteVideoById(int id) {
        int rows = namedParameterJdbcTemplate.update("delete from videos where id = :id", new MapSqlParameterSource("id", id));
        if (rows == 1) {
            LOGGER.info("Video with id {} was deleted", id);
        } else {
            LOGGER.error("Something went wrong during deleting");
        }
        return rows;
    }

    @Override
    public void saveVideo(Video video) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("videoName", video.getVideoName())
                .addValue("videoType", video.getVideoType())
                .addValue("videoData", video.getVideoData());
        namedParameterJdbcTemplate.update("insert into videos (video_name, video_type, video_data) values (:videoName, :videoType, :videoData)", params);
        LOGGER.info("New Video {} was inserted", video);
    }

    @Override
    public int countVideos() {
        return namedParameterJdbcTemplate.queryForObject("select count(*) from videos", (HashMap) null, Integer.class);
    }

    @Override
    public List<Video> findAllFavoriteVideos(ArrayList<Integer> videosId) {
        LOGGER.debug("Finding all favorite videos");
        List<Video> favoriteVideos = new ArrayList<>();
        if (videosId != null) {
            String sqlQueryForFavoriteVideos = "select * from videos where id in (:videosId)";
            SqlParameterSource namedParameters = new MapSqlParameterSource("videosId", videosId);
            try {
                favoriteVideos = namedParameterJdbcTemplate.query(sqlQueryForFavoriteVideos, namedParameters, VIDEO_ROW_MAPPER);
            } catch (DataAccessException dataAccessException) {
                LOGGER.error("Empty result");
            }
        }
        return favoriteVideos;
    }
}