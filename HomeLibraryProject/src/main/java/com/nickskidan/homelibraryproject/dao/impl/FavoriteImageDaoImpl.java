package com.nickskidan.homelibraryproject.dao.impl;

import com.nickskidan.homelibraryproject.dao.api.FavoriteImageDao;
import com.nickskidan.homelibraryproject.model.FavoriteImage;
import com.nickskidan.homelibraryproject.model.Image;
import com.nickskidan.homelibraryproject.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Repository(value = "favoriteImageDao")
public class FavoriteImageDaoImpl implements FavoriteImageDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(FavoriteImageDaoImpl.class);

    private JdbcTemplate jdbcTemplate;

    @Autowired
    FavoriteImageDaoImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public void insertFavoriteImage(User user, Image image) {
        jdbcTemplate.update("insert into favorite_images values(?, ?)", user.getId(), image.getId());
        LOGGER.info("New data was inserted into favorite_images");
    }

    @Override
    public ArrayList<Integer> findAllImagesIdByUser(User user) {
        LOGGER.debug("Finding all favorite images id's of User {} ", user.getLogin());
        ArrayList<Integer> imagesIdList = new ArrayList<>();
        String sql = "select * from favorite_images where user_id = " + user.getId();
        try {
            List<FavoriteImage> favoriteImages = jdbcTemplate.query(sql, FAVORITE_IMAGE_ROW_MAPPER);
            for (FavoriteImage favoriteImage : favoriteImages) {
                imagesIdList.add(favoriteImage.getImageId());
            }
            LOGGER.info("Id's of user's favorite images are {} ", imagesIdList);
        } catch (DataAccessException dataAccessException) {
            LOGGER.error("Empty result");
        }
        return imagesIdList.size() > 0 ? imagesIdList : null;
    }

    @Override
    public int deleteFromFavoriteByImageId(int id) {
        int rows = jdbcTemplate.update("delete from favorite_images where image_id = ?", id);
        if (rows == 1) {
            LOGGER.info("Image with id {} was deleted from favorite", id);
        } else {
            LOGGER.error("Something went wrong during deleting");
        }
        return rows;
    }

    @Override
    public FavoriteImage findByUserIdAndImageId(User user, Image image) {
        LOGGER.debug("Finding favoriteImage, where user_id {} and image_id {}", user.getId(), image.getId());
        FavoriteImage favoriteImage = null;
        try {
            favoriteImage = jdbcTemplate.queryForObject("select * from favorite_images where user_id = ? and image_id = ?", new Object[]{user.getId(), image.getId()}, FAVORITE_IMAGE_ROW_MAPPER);
        } catch (DataAccessException dataAccessException) {
            LOGGER.error("Couldn't find favoriteImage");
        }
        return favoriteImage;
    }
}