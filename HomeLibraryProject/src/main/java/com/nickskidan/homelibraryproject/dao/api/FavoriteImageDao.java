package com.nickskidan.homelibraryproject.dao.api;

import com.nickskidan.homelibraryproject.model.FavoriteImage;
import com.nickskidan.homelibraryproject.model.Image;
import com.nickskidan.homelibraryproject.model.User;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.util.ArrayList;

public interface FavoriteImageDao {
    RowMapper<FavoriteImage> FAVORITE_IMAGE_ROW_MAPPER = (ResultSet rs, int rowNum) ->
            new FavoriteImage(rs.getInt("user_id"), rs.getInt("image_id"));

    void insertFavoriteImage(User user, Image image);

    ArrayList<Integer> findAllImagesIdByUser(User user);

    int deleteFromFavoriteByImageId(int id);

    FavoriteImage findByUserIdAndImageId(User user, Image image);
}