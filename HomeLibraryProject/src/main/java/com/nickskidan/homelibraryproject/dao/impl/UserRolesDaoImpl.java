package com.nickskidan.homelibraryproject.dao.impl;

import com.nickskidan.homelibraryproject.dao.api.UserRolesDao;
import com.nickskidan.homelibraryproject.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;

@Repository(value = "userRolesDao")
public class UserRolesDaoImpl implements UserRolesDao {

    private static final int ID_ROLE_USER = 2;

    private static final Logger LOGGER = LoggerFactory.getLogger(UserDaoImpl.class);

    private JdbcTemplate jdbcTemplate;

    @Autowired
    UserRolesDaoImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public void insertUserRoles(User user) {
        jdbcTemplate.update("insert into user_roles values(?, ?)", user.getId(), ID_ROLE_USER);
        LOGGER.info("New data was inserted into user_roles");
    }
}