package com.nickskidan.homelibraryproject.controller;

import com.nickskidan.homelibraryproject.model.User;
import com.nickskidan.homelibraryproject.model.Video;
import com.nickskidan.homelibraryproject.service.api.FavoriteVideoService;
import com.nickskidan.homelibraryproject.service.api.UserService;
import com.nickskidan.homelibraryproject.service.api.VideoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
public class VideoController {

    private static final Logger LOGGER = LoggerFactory.getLogger(VideoController.class);

    private static final String VIDEOS_PAGE = "videos";
    private static final String REDIRECT_VIDEOS_PAGE = "redirect:/videos";
    private static final String VIDEOS_TYPE = "video/mp4";
    private static final String UPLOAD_VIDEO_PAGE = "upload_video";
    private static final String UPLOAD_VIDEO_SUCCESS_PAGE = "upload_video_success";
    private static final String REDIRECT_UPLOAD_VIDEO_SUCCESS_PAGE = "redirect:/upload_video_success";

    private VideoService videoService;
    private FavoriteVideoService favoriteVideoService;
    private UserService userService;

    @Autowired
    public VideoController(VideoService videoService, FavoriteVideoService favoriteVideoService, UserService userService) {
        this.videoService = videoService;
        this.favoriteVideoService = favoriteVideoService;
        this.userService = userService;
    }

    @GetMapping("/videos")
    public String videos(@RequestParam(defaultValue = "asc") String sortOrder,
                         @RequestParam(defaultValue = "5") int videosQuantityOnPage,
                         @RequestParam(defaultValue = "1") int page, Model model) {
        float videosQuantity = videoService.countVideos();
        int pagesQuantity = (int) (Math.ceil(videosQuantity / videosQuantityOnPage));
        List<Video> videosList = videoService.getAllVideosSortedByNameWithPagination(sortOrder, videosQuantityOnPage, page);
        model.addAttribute("videosQuantity", (int) videosQuantity);
        if (videosList != null && !videosList.isEmpty()) {
            model.addAttribute("sortOrder", sortOrder);
            model.addAttribute("reverseSortOrder", sortOrder.equals("asc") ? "desc" : "asc");
            model.addAttribute("videosQuantityOnPage", videosQuantityOnPage);
            model.addAttribute("page", page);
            model.addAttribute("pagesQuantity", pagesQuantity);
            model.addAttribute("videosList", videosList);
        } else {
            model.addAttribute("emptyList", "No videos in home library, but you can add a new video");
        }
        return VIDEOS_PAGE;
    }

    @GetMapping(value = "/video/{id}", produces = VIDEOS_TYPE)
    public @ResponseBody
    byte[] video(@PathVariable Integer id) {
        Video video = videoService.findById(id);
        byte[] emptyArray = new byte[0];
        if (video != null) {
            LOGGER.info("Showing Video with id {}", id);
            return video.getVideoData();
        } else {
            LOGGER.error("Trying to show not present in library video. Returning empty byte array to avoid NPE");
            return emptyArray;
        }
    }

    @GetMapping("/download_video/{id}")
    public ResponseEntity downloadVideo(@PathVariable Integer id) {
        Video video = videoService.findById(id);
        if (video != null) {
            LOGGER.info("Downloading Video with id {}", id);
            return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType(VIDEOS_TYPE))
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""
                            + video.getVideoName() + "\"")
                    .body(video.getVideoData());
        } else {
            LOGGER.error("Trying to download not present in library video. Returning empty ResponseEntity to avoid NPE");
            return ResponseEntity.noContent().build();
        }
    }

    @DeleteMapping("/video/{id}")
    public String deleteVideo(@PathVariable int id, RedirectAttributes redirectAttributes) {
        String videoName = videoService.findById(id).getVideoName();
        LOGGER.debug("Deleting Video {} ", videoName);
        int deletedRows = videoService.deleteVideoById(id);
        redirectAttributes.addFlashAttribute("deletedRows", deletedRows);
        redirectAttributes.addFlashAttribute("videoName", videoName);
        return REDIRECT_VIDEOS_PAGE;
    }

    @GetMapping("/upload_video_success")
    public String uploadVideoSuccess() {
        LOGGER.info("Redirecting to upload_video_success page");
        return UPLOAD_VIDEO_SUCCESS_PAGE;
    }

    @GetMapping("/upload_video")
    public String uploadVideo() {
        LOGGER.info("You are on upload_video page");
        return UPLOAD_VIDEO_PAGE;
    }

    @PostMapping("/upload_video")
    public String uploadVideo(@RequestParam("file") MultipartFile file, Model model, RedirectAttributes redirectAttributes) {
        if (file.isEmpty()) {
            LOGGER.info("Trying upload empty video");
            model.addAttribute("emptyFile", "Video for uploading is not selected");
            return UPLOAD_VIDEO_PAGE;
        }
        if (!VIDEOS_TYPE.equals(file.getContentType())) {
            LOGGER.info("Trying upload not video");
            model.addAttribute("wrongType", "Chosen file has incorrect type");
            return UPLOAD_VIDEO_PAGE;
        }
        videoService.saveVideo(file);
        LOGGER.info("Video {} was successfully uploaded", file.getOriginalFilename());
        redirectAttributes.addFlashAttribute("file", file.getOriginalFilename());
        return REDIRECT_UPLOAD_VIDEO_SUCCESS_PAGE;
    }

    @GetMapping("favorite_video/{id}")
    public String addVideoToFavorite(@PathVariable int id, @AuthenticationPrincipal UserDetails user, RedirectAttributes redirectAttributes) {
        User currentUser = userService.findByLogin(user.getUsername());
        Video video = videoService.findById(id);
        if (video != null) {
            if (favoriteVideoService.findByUserIdAndVideoId(currentUser, video) != null) {
                LOGGER.info("User {} has already added video {} to favorite_list", currentUser.getLogin(), video.getVideoName());
                redirectAttributes.addFlashAttribute("videoTitle", video.getVideoName());
                redirectAttributes.addFlashAttribute("repeatedAddition", "has been already added to your favorite earlier");
            } else {
                LOGGER.info("Adding video with id {} to a favorite_list", id);
                favoriteVideoService.insertFavoriteVideo(currentUser, video);
            }
        } else {
            redirectAttributes.addFlashAttribute("nullVideo", "You are trying to add to your favorite not present in library video");
        }
        return REDIRECT_VIDEOS_PAGE;
    }
}