package com.nickskidan.homelibraryproject.controller;

import com.nickskidan.homelibraryproject.model.Image;
import com.nickskidan.homelibraryproject.model.TextFile;
import com.nickskidan.homelibraryproject.model.User;
import com.nickskidan.homelibraryproject.model.Video;
import com.nickskidan.homelibraryproject.service.api.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Controller
public class FavoriteFileController {

    private static final Logger LOGGER = LoggerFactory.getLogger(FavoriteFileController.class);

    private static final String FAVORITE_LIST_PAGE = "favorite_list";
    private static final String REDIRECT_FAVORITE_LIST_PAGE = "redirect:/favorite_list";

    private ImageService imageService;
    private TextFileService textFileService;
    private VideoService videoService;
    private FavoriteImageService favoriteImageService;
    private FavoriteTextFileService favoriteTextFileService;
    private FavoriteVideoService favoriteVideoService;
    private UserService userService;

    @Autowired
    FavoriteFileController(ImageService imageService, TextFileService textFileService, VideoService videoService,
                           FavoriteImageService favoriteImageService, FavoriteTextFileService favoriteTextFileService, FavoriteVideoService favoriteVideoService,
                           UserService userService) {
        this.imageService = imageService;
        this.textFileService = textFileService;
        this.videoService = videoService;
        this.favoriteImageService = favoriteImageService;
        this.favoriteTextFileService = favoriteTextFileService;
        this.favoriteVideoService = favoriteVideoService;
        this.userService = userService;
    }

    @GetMapping("/favorite_list")
    public String favoriteFilesList(@AuthenticationPrincipal UserDetails user, Model model) {
        User currentUser = userService.findByLogin(user.getUsername());
        LOGGER.info("Finding all user's favorite files");
        List<Image> favoriteImages = imageService.findAllFavoriteImages(favoriteImageService.findAllImagesIdByUser(currentUser));
        List<TextFile> favoriteTextFiles = textFileService.findAllFavoriteTextFiles(favoriteTextFileService.findAllTextFilesIdByUser(currentUser));
        List<Video> favoriteVideos = videoService.findAllFavoriteVideos(favoriteVideoService.findAllVideosIdByUser(currentUser));
        List<Object> favoriteFiles = Stream.of(favoriteImages, favoriteTextFiles, favoriteVideos)
                .flatMap(List::stream)
                .collect(Collectors.toList());
        model.addAttribute("favoriteFiles", favoriteFiles);
        return FAVORITE_LIST_PAGE;
    }

    @DeleteMapping("/favorite_list/image/{id}")
    public String deleteImageFromFavorite(@PathVariable int id, RedirectAttributes redirectAttributes) {
        Image image = imageService.findById(id);
        if (image != null) {
            LOGGER.info("Deleting image {} from favorite", image.getImageName());
            int imageDeletedRows = favoriteImageService.deleteFromFavoriteByImageId(id);
            redirectAttributes.addFlashAttribute("imageDeletedRows", imageDeletedRows);
            redirectAttributes.addFlashAttribute("imageName", image.getImageName());
        } else {
            LOGGER.error("Trying delete not present in images image from favorite_list");
            redirectAttributes.addFlashAttribute("imageError", "You are trying to delete not present in library image from favorite_list");
        }
        return REDIRECT_FAVORITE_LIST_PAGE;
    }

    @DeleteMapping("/favorite_list/text_file/{id}")
    public String deleteTextFileFromFavorite(@PathVariable int id, RedirectAttributes redirectAttributes) {
        TextFile textFile = textFileService.findById(id);
        if (textFile != null) {
            LOGGER.info("Deleting textFile {} from favorite", textFile.getTextFileName());
            int textFileDeletedRows = favoriteTextFileService.deleteFromFavoriteByTextFileId(id);
            redirectAttributes.addFlashAttribute("textFileDeletedRows", textFileDeletedRows);
            redirectAttributes.addFlashAttribute("textFileName", textFile.getTextFileName());
        } else {
            LOGGER.error("Trying delete not present in text_files textFile from favorite_list");
            redirectAttributes.addFlashAttribute("textFileError", "You are trying to delete not present in library textFile from favorite_list");
        }
        return REDIRECT_FAVORITE_LIST_PAGE;
    }

    @DeleteMapping("/favorite_list/video/{id}")
    public String deleteVideoFromFavorite(@PathVariable int id, RedirectAttributes redirectAttributes) {
        Video video = videoService.findById(id);
        if (video != null) {
            LOGGER.info("Deleting video {} from favorite", videoService.findById(id).getVideoName());
            int videoDeletedRows = favoriteVideoService.deleteFromFavoriteByVideoId(id);
            redirectAttributes.addFlashAttribute("videoDeletedRows", videoDeletedRows);
            redirectAttributes.addFlashAttribute("videoName", video.getVideoName());
        } else {
            LOGGER.error("Trying delete not present in videos video from favorite_list");
            redirectAttributes.addFlashAttribute("videoError", "You are trying to delete not present in library video from favorite_list");
        }
        return REDIRECT_FAVORITE_LIST_PAGE;
    }
}