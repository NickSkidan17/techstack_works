package com.nickskidan.homelibraryproject.controller;

import com.nickskidan.homelibraryproject.model.*;
import com.nickskidan.homelibraryproject.service.api.FavoriteImageService;
import com.nickskidan.homelibraryproject.service.api.ImageService;
import com.nickskidan.homelibraryproject.service.api.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.List;

@Controller
public class ImageController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ImageController.class);

    private static final String IMAGES_PAGE = "images";
    private static final String REDIRECT_IMAGES_PAGE = "redirect:/images";
    private static final String IMAGES_TYPE = "image/jpeg";
    private static final String UPLOAD_IMAGE_PAGE = "upload_image";
    private static final String UPLOAD_IMAGE_SUCCESS_PAGE = "upload_image_success";
    private static final String REDIRECT_UPLOAD_IMAGE_SUCCESS_PAGE = "redirect:/upload_image_success";

    private ImageService imageService;
    private FavoriteImageService favoriteImageService;
    private UserService userService;

    @Autowired
    public ImageController(ImageService imageService, FavoriteImageService favoriteImageService, UserService userService) {
        this.imageService = imageService;
        this.favoriteImageService = favoriteImageService;
        this.userService = userService;
    }

    @GetMapping("/images")
    public String images(@RequestParam(defaultValue = "asc") String sortOrder,
                         @RequestParam(defaultValue = "5") int imagesQuantityOnPage,
                         @RequestParam(defaultValue = "1") int page, Model model) {
        float imagesQuantity = imageService.countImages();
        int pagesQuantity = (int) (Math.ceil(imagesQuantity / imagesQuantityOnPage));
        List<Image> imagesList = imageService.getAllImagesSortedByNameWithPagination(sortOrder, imagesQuantityOnPage, page);
        model.addAttribute("imagesQuantity", (int) imagesQuantity);
        if (imagesList != null && !imagesList.isEmpty()) {
            model.addAttribute("sortOrder", sortOrder);
            model.addAttribute("reverseSortOrder", sortOrder.equals("asc") ? "desc" : "asc");
            model.addAttribute("imagesQuantityOnPage", imagesQuantityOnPage);
            model.addAttribute("page", page);
            model.addAttribute("pagesQuantity", pagesQuantity);
            model.addAttribute("imagesList", imagesList);
        } else {
            model.addAttribute("emptyList", "No images in home library, but you can add a new image");
        }
        return IMAGES_PAGE;
    }

    @GetMapping(value = "/image/{id}", produces = IMAGES_TYPE)
    public @ResponseBody
    byte[] image(@PathVariable Integer id) {
        Image image = imageService.findById(id);
        byte[] emptyArray = new byte[0];
        if (image != null) {
            LOGGER.info("Showing Image with id {}", id);
            return image.getImageData();
        } else {
            LOGGER.error("Trying to show not present in library image. Returning empty byte array to avoid NPE");
            return emptyArray;
        }
    }

    @GetMapping("/download_image/{id}")
    public ResponseEntity downloadImage(@PathVariable Integer id) {
        Image image = imageService.findById(id);
        if (image != null) {
            LOGGER.info("Downloading Image with id {}", id);
            return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType(IMAGES_TYPE))
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""
                            + image.getImageName() + "\"")
                    .body(image.getImageData());
        } else {
            LOGGER.error("Trying to download not present in library image. Returning empty ResponseEntity to avoid NPE");
            return ResponseEntity.noContent().build();
        }
    }

    @DeleteMapping("/image/{id}")
    public String deleteImage(@PathVariable int id, RedirectAttributes redirectAttribute) {
        String imageName = imageService.findById(id).getImageName();
        LOGGER.debug("Deleting Image {} ", imageName);
        int deletedRows = imageService.deleteImageById(id);
        redirectAttribute.addFlashAttribute("deletedRows", deletedRows);
        redirectAttribute.addFlashAttribute("imageName", imageName);
        return REDIRECT_IMAGES_PAGE;
    }

    @GetMapping("/upload_image_success")
    public String uploadImageSuccess() {
        LOGGER.info("Redirecting to upload_image_success page");
        return UPLOAD_IMAGE_SUCCESS_PAGE;
    }

    @GetMapping("/upload_image")
    public String uploadImage() {
        LOGGER.info("You are on upload_image page");
        return UPLOAD_IMAGE_PAGE;
    }

    @PostMapping("/upload_image")
    public String uploadImage(@RequestParam("file") MultipartFile file, Model model, RedirectAttributes redirectAttributes) {
        if (file.isEmpty()) {
            LOGGER.info("Trying upload empty image");
            model.addAttribute("emptyFile", "Image for uploading is not selected");
            return UPLOAD_IMAGE_PAGE;
        }
        if (!IMAGES_TYPE.equals(file.getContentType()) && !("image/png").equals(file.getContentType())) {
            LOGGER.info("Trying upload not image");
            model.addAttribute("wrongType", "Chosen file has incorrect type");
            return UPLOAD_IMAGE_PAGE;
        }
        imageService.saveImage(file);
        LOGGER.info("Image {} was successfully uploaded", file.getOriginalFilename());
        redirectAttributes.addFlashAttribute("file", file.getOriginalFilename());
        return REDIRECT_UPLOAD_IMAGE_SUCCESS_PAGE;
    }

    @GetMapping("favorite_image/{id}")
    public String addImageToFavorite(@PathVariable int id, @AuthenticationPrincipal UserDetails user, RedirectAttributes redirectAttributes) {
        User currentUser = userService.findByLogin(user.getUsername());
        Image image = imageService.findById(id);
        if (image != null) {
            if (favoriteImageService.findByImageIdAndUserId(currentUser, image) != null) {
                LOGGER.info("User {} has already added image {} to favorite_list", currentUser.getLogin(), image.getImageName());
                redirectAttributes.addFlashAttribute("imageTitle", image.getImageName());
                redirectAttributes.addFlashAttribute("repeatedAddition", "has been already added to your favorite earlier");
            } else {
                LOGGER.info("Adding image with id {} to a favorite_list", id);
                favoriteImageService.insertFavoriteImage(currentUser, image);
            }
        } else {
            redirectAttributes.addFlashAttribute("nullImage", "You are trying to add to your favorite not present in library image");
        }
        return REDIRECT_IMAGES_PAGE;
    }
}