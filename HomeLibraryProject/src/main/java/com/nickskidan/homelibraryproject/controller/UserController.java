package com.nickskidan.homelibraryproject.controller;

import com.nickskidan.homelibraryproject.model.User;
import com.nickskidan.homelibraryproject.service.api.SecurityService;
import com.nickskidan.homelibraryproject.service.api.UserService;
import com.nickskidan.homelibraryproject.validator.ValidatorForCreatingUser;
import com.nickskidan.homelibraryproject.validator.ValidatorForUpdatingUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class UserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    private static final String REGISTRATION_PAGE = "registration";
    private static final String HOME_PAGE = "home_page";
    private static final String REDIRECT_HOME_PAGE = "redirect:/home_page";
    private static final String LOGIN_PAGE = "login";
    private static final String REDIRECT_LOGIN_PAGE = "redirect:/login?logout";
    private static final String ACCOUNT_PAGE = "account";
    private static final String UPDATE_USER_PAGE = "update_user";

    private UserService userService;
    private SecurityService securityService;
    private ValidatorForCreatingUser validatorForCreatingUser;
    private ValidatorForUpdatingUser validatorForUpdatingUser;

    @Autowired
    UserController(UserService userService, SecurityService securityService, ValidatorForCreatingUser validatorForCreatingUser, ValidatorForUpdatingUser validatorForUpdatingUser) {
        this.userService = userService;
        this.securityService = securityService;
        this.validatorForCreatingUser = validatorForCreatingUser;
        this.validatorForUpdatingUser = validatorForUpdatingUser;
    }

    @GetMapping("/registration")
    public String registration(Model model) {
        model.addAttribute("user", new User());
        LOGGER.info("Redirecting to registration page");
        return REGISTRATION_PAGE;
    }

    @PostMapping("/registration")
    public String registration(@ModelAttribute("user") User user, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        validatorForCreatingUser.validate(user, bindingResult);
        if (bindingResult.hasErrors()) {
            LOGGER.info("Some errors during registration");
            return REGISTRATION_PAGE;
        }
        userService.saveUser(user);
        securityService.autoLogin(user.getLogin(), user.getConfirmPassword());
        LOGGER.info("User was successfully created");
        return REDIRECT_HOME_PAGE;
    }

    @GetMapping("/login")
    public String login(Model model, String error, String logout) {
        if (error != null) {
            model.addAttribute("error", "Login or password is incorrect.");
            LOGGER.info("Login or password is incorrect.");
        }
        if (logout != null) {
            model.addAttribute("message", "You have been logged out.");
            LOGGER.info("Successfully logged out.");
        }
        return LOGIN_PAGE;
    }

    @GetMapping({"/", "/home_page"})
    public String welcome() {
        LOGGER.info("Redirecting to home page");
        return HOME_PAGE;
    }

    @GetMapping("/account")
    public String showCurrentUser(@AuthenticationPrincipal UserDetails user, Model model) {
        User currentUser = userService.findByLogin(user.getUsername());
        LOGGER.info("User {} will be updated", currentUser.getLogin());
        model.addAttribute("user", currentUser);
        return ACCOUNT_PAGE;
    }

    @GetMapping("/user")
    public String editUser(@AuthenticationPrincipal UserDetails user, Model model) {
        User currentUser = userService.findByLogin(user.getUsername());
        LOGGER.info("User {} will be updated", currentUser);
        model.addAttribute("user", currentUser);
        return UPDATE_USER_PAGE;
    }

    @PutMapping("/user")
    public String editUser(@ModelAttribute("user") User user, BindingResult bindingResult, RedirectAttributes
            redirectAttributes) {
        validatorForUpdatingUser.validate(user, bindingResult);
        if (bindingResult.hasErrors()) {
            LOGGER.info("Some errors during updating");
            return UPDATE_USER_PAGE;
        }
        user.setId(securityService.findLoggedInUser().getId());
        LOGGER.info("Updating User with id {} ", user.getId());
        User userAfterUpdating = userService.updateUser(user);
        redirectAttributes.addFlashAttribute("userAfterUpdating", userAfterUpdating);
        return REDIRECT_LOGIN_PAGE;
    }
}