package com.nickskidan.homelibraryproject.controller;

import com.nickskidan.homelibraryproject.model.TextFile;
import com.nickskidan.homelibraryproject.model.User;
import com.nickskidan.homelibraryproject.service.api.FavoriteTextFileService;
import com.nickskidan.homelibraryproject.service.api.TextFileService;
import com.nickskidan.homelibraryproject.service.api.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
public class TextFileController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TextFileController.class);

    private static final String TEXT_FILES_PAGE = "text_files";
    private static final String REDIRECT_TEXT_FILES_PAGE = "redirect:/text_files";
    private static final String TEXT_FILES_TYPE = "text/plain";
    private static final String UPLOAD_TEXT_FILE_PAGE = "upload_text_file";
    private static final String UPLOAD_TEXT_FILE_SUCCESS_PAGE = "upload_text_file_success";
    private static final String REDIRECT_UPLOAD_TEXT_FILE_SUCCESS_PAGE = "redirect:/upload_text_file_success";

    private TextFileService textFileService;
    private FavoriteTextFileService favoriteTextFileService;
    private UserService userService;

    @Autowired
    public TextFileController(TextFileService textFileService, FavoriteTextFileService favoriteTextFileService, UserService userService) {
        this.textFileService = textFileService;
        this.favoriteTextFileService = favoriteTextFileService;
        this.userService = userService;
    }

    @GetMapping("/text_files")
    public String textFiles(@RequestParam(defaultValue = "asc") String sortOrder,
                            @RequestParam(defaultValue = "5") int textFilesQuantityOnPage,
                            @RequestParam(defaultValue = "1") int page, Model model) {
        float textFilesQuantity = textFileService.countTextFiles();
        int pagesQuantity = (int) (Math.ceil(textFilesQuantity / textFilesQuantityOnPage));
        List<TextFile> textFilesList = textFileService.getAllTextFilesSortedByNameWithPagination(sortOrder, textFilesQuantityOnPage, page);
        model.addAttribute("textFilesQuantity", (int) textFilesQuantity);
        if (textFilesList != null && !textFilesList.isEmpty()) {
            model.addAttribute("sortOrder", sortOrder);
            model.addAttribute("reverseSortOrder", sortOrder.equals("asc") ? "desc" : "asc");
            model.addAttribute("textFilesQuantityOnPage", textFilesQuantityOnPage);
            model.addAttribute("page", page);
            model.addAttribute("pagesQuantity", pagesQuantity);
            model.addAttribute("textFilesList", textFilesList);
        } else {
            model.addAttribute("emptyList", "No textFiles in home library, but you can add a new textFile");
        }
        return TEXT_FILES_PAGE;
    }

    @GetMapping(value = "/text_file/{id}", produces = TEXT_FILES_TYPE)
    public @ResponseBody
    byte[] textFile(@PathVariable Integer id) {
        TextFile textFile = textFileService.findById(id);
        byte[] emptyArray = new byte[0];
        if (textFile != null) {
            LOGGER.info("Showing TextFile with id {}", id);
            return textFile.getTextFileData();
        } else {
            LOGGER.error("Trying to show not present in library textFile. Returning empty byte array to avoid NPE");
            return emptyArray;
        }
    }

    @GetMapping("/download_text_file/{id}")
    public ResponseEntity downloadTextFile(@PathVariable Integer id) {
        TextFile textFile = textFileService.findById(id);
        if (textFile != null) {
            LOGGER.info("Downloading TextFile with id {}", id);
            return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType(TEXT_FILES_TYPE))
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""
                            + textFile.getTextFileName() + "\"")
                    .body(textFile.getTextFileData());
        } else {
            LOGGER.error("Trying to download not present in library textFile. Returning empty ResponseEntity to avoid NPE");
            return ResponseEntity.noContent().build();
        }
    }

    @DeleteMapping("/text_file/{id}")
    public String deleteTextFile(@PathVariable int id, RedirectAttributes redirectAttributes) {
        String textFileName = textFileService.findById(id).getTextFileName();
        LOGGER.debug("Deleting TextFile {} ", textFileName);
        int deletedRows = textFileService.deleteTextFileById(id);
        redirectAttributes.addFlashAttribute("deletedRows", deletedRows);
        redirectAttributes.addFlashAttribute("textFileName", textFileName);
        return REDIRECT_TEXT_FILES_PAGE;
    }

    @GetMapping("/upload_text_file_success")
    public String uploadTextFileSuccess() {
        LOGGER.info("Redirecting to upload_text_file_page");
        return UPLOAD_TEXT_FILE_SUCCESS_PAGE;
    }

    @GetMapping("/upload_text_file")
    public String uploadTextFile() {
        LOGGER.info("You are on upload_text_file page");
        return UPLOAD_TEXT_FILE_PAGE;
    }

    @PostMapping("/upload_text_file")
    public String uploadTextFile(@RequestParam("file") MultipartFile file, Model model, RedirectAttributes redirectAttributes) {
        if (file.isEmpty()) {
            LOGGER.info("Trying upload empty textFile");
            model.addAttribute("emptyFile", "TextFile for uploading is not selected");
            return UPLOAD_TEXT_FILE_PAGE;
        }
        if (!TEXT_FILES_TYPE.equals(file.getContentType())) {
            LOGGER.info("Trying upload not textFile");
            model.addAttribute("wrongType", "Chosen file has incorrect type");
            return UPLOAD_TEXT_FILE_PAGE;
        }
        textFileService.saveTextFile(file);
        LOGGER.info("TextFile {} was successfully uploaded", file.getOriginalFilename());
        redirectAttributes.addFlashAttribute("file", file.getOriginalFilename());
        return REDIRECT_UPLOAD_TEXT_FILE_SUCCESS_PAGE;
    }

    @GetMapping("favorite_text_file/{id}")
    public String addTextFileToFavorite(@PathVariable int id, @AuthenticationPrincipal UserDetails user, RedirectAttributes redirectAttributes) {
        User currentUser = userService.findByLogin(user.getUsername());
        TextFile textFile = textFileService.findById(id);
        if (textFile != null) {
            if (favoriteTextFileService.findByUserIdAndTextFileId(currentUser, textFile) != null) {
                LOGGER.info("User {} has already added text_file {} to favorite_list", currentUser.getLogin(), textFile.getTextFileName());
                redirectAttributes.addFlashAttribute("textFileTitle", textFile.getTextFileName());
                redirectAttributes.addFlashAttribute("repeatedAddition", "has been already added to your favorite earlier");
            } else {
                LOGGER.info("Adding textFile with id {} to a favorite_list", id);
                favoriteTextFileService.insertFavoriteTextFile(currentUser, textFile);
            }
        } else {
            redirectAttributes.addFlashAttribute("nullTextFile", "You are trying to add to your favorite not present in library textFile");
        }
        return REDIRECT_TEXT_FILES_PAGE;
    }
}