package com.nickskidan.homelibraryproject.controller;

import com.nickskidan.homelibraryproject.model.User;
import com.nickskidan.homelibraryproject.service.api.UserService;
import com.nickskidan.homelibraryproject.validator.ValidatorForCreatingUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
public class AdminController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AdminController.class);

    private static final String ADMIN_PAGE = "admin";
    private static final String REDIRECT_ADMIN_PAGE = "redirect:/admin";
    private static final String ADD_USER_PAGE = "add_user";
    private static final String ADD_USER_SUCCESS_PAGE = "add_user_success";
    private static final String ACCESS_DENIED_PAGE = "access_denied";

    private UserService userService;
    private ValidatorForCreatingUser validatorForCreatingUser;

    @Autowired
    AdminController(UserService userService, ValidatorForCreatingUser validatorForCreatingUser) {
        this.userService = userService;
        this.validatorForCreatingUser = validatorForCreatingUser;
    }

    @GetMapping("/admin")
    public String users(@RequestParam(defaultValue = "asc") String sortOrder,
                        @RequestParam(defaultValue = "5") int usersQuantityOnPage,
                        @RequestParam(defaultValue = "1") int page, Model model) {
        float usersQuantity = userService.countUsers();
        int pagesQuantity = (int) (Math.ceil(usersQuantity / usersQuantityOnPage));
        List<User> users = userService.getAllUsersSortedByLoginWithPagination(sortOrder, usersQuantityOnPage, page);
        model.addAttribute("usersQuantity", (int) usersQuantity);
        if (users != null && !users.isEmpty()) {
            model.addAttribute("sortOrder", sortOrder);
            model.addAttribute("reverseSortOrder", sortOrder.equals("asc") ? "desc" : "asc");
            model.addAttribute("usersQuantityOnPage", usersQuantityOnPage);
            model.addAttribute("page", page);
            model.addAttribute("pagesQuantity", pagesQuantity);
            model.addAttribute("users", users);
        } else {
            model.addAttribute("emptyList", "No users at home library");
        }
        return ADMIN_PAGE;
    }

    @DeleteMapping("/admin/{id}")
    public String deleteUser(@PathVariable int id, RedirectAttributes redirectAttributes) {
        String login = userService.findById(id).getLogin();
        LOGGER.debug("Deleting User {}", login);
        int deletedRows = userService.deleteUserById(id);
        redirectAttributes.addFlashAttribute("deletedRows", deletedRows);
        redirectAttributes.addFlashAttribute("login", login);
        return REDIRECT_ADMIN_PAGE;
    }

    @GetMapping("/admin/user")
    public String addUser(User user) {
        LOGGER.info("You are on add_user page");
        return ADD_USER_PAGE;
    }

    @PostMapping("/admin/user")
    public String addUser(User user, BindingResult bindingResult, Model model) {
        validatorForCreatingUser.validate(user, bindingResult);
        if (bindingResult.hasErrors()) {
            LOGGER.info("Some errors during adding user");
            return ADD_USER_PAGE;
        }
        userService.saveUser(user);
        LOGGER.info("User {} was added", user.getLogin());
        model.addAttribute("success", "User " + user.getLogin() + " was added successfully");
        return ADD_USER_SUCCESS_PAGE;
    }

    @GetMapping("/access_denied")
    public String accessDenied(Model model) {
        LOGGER.info("Access denied for current user");
        return ACCESS_DENIED_PAGE;
    }
}