package com.nickskidan.homelibraryproject.validator;

import com.nickskidan.homelibraryproject.dao.api.UserDao;
import com.nickskidan.homelibraryproject.model.User;
import com.nickskidan.homelibraryproject.service.api.SecurityService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class ValidatorForUpdatingUser implements Validator {

    private static final String VALID_EMAIL = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";

    private UserDao userDao;
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    private SecurityService securityService;

    ValidatorForUpdatingUser(UserDao userDao, BCryptPasswordEncoder bCryptPasswordEncoder, SecurityService securityService) {
        this.userDao = userDao;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.securityService = securityService;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return User.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        User user = (User) o;
        User userFindByLogin = userDao.findByLogin(user.getLogin());
        User userFindByEmail = userDao.findByEmail(user.getEmail());

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "login", "Required");
        if (user.getLogin().length() < 4 || user.getLogin().length() > 32) {
            errors.rejectValue("login", "Size.user.login");
        }

        if (userFindByLogin != null) {
            if (!user.getLogin().equals(securityService.findLoggedInUser().getLogin())) {
                errors.rejectValue("login", "Duplicate.user.login");
            }
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "Required");
        if (user.getPassword().length() < 6 || user.getPassword().length() > 32) {
            errors.rejectValue("password", "Size.user.password");
        }
        if (!bCryptPasswordEncoder.matches(user.getCurrentPassword(), securityService.findLoggedInUser().getPassword())) {
            errors.rejectValue("currentPassword", "Wrong.user.currentPassword");
        }

        if (!user.getConfirmPassword().equals(user.getPassword())) {
            errors.rejectValue("confirmPassword", "Different.user.password");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "Empty.user.email");
        Pattern pattern = Pattern.compile(VALID_EMAIL);
        Matcher matcher = pattern.matcher(user.getEmail());
        if (!matcher.matches()) {
            errors.rejectValue("email", "Valid.user.email");
        }

        if (userFindByEmail != null) {
            if (!user.getEmail().equals(securityService.findLoggedInUser().getEmail())) {
                errors.rejectValue("email", "Duplicate.user.email");
            }
        }
    }
}