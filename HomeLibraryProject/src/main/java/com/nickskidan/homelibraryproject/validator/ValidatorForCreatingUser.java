package com.nickskidan.homelibraryproject.validator;

import com.nickskidan.homelibraryproject.dao.api.UserDao;
import com.nickskidan.homelibraryproject.model.User;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class ValidatorForCreatingUser implements Validator {

    private static final String VALID_EMAIL = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";

    private UserDao userDao;

    ValidatorForCreatingUser(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return User.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        User user = (User) o;
        User userFindByLogin = userDao.findByLogin(user.getLogin());
        User userFindByEmail = userDao.findByEmail(user.getEmail());

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "login", "Required");
        if (user.getLogin().length() < 4 || user.getLogin().length() > 32) {
            errors.rejectValue("login", "Size.user.login");
        }

        if (userFindByLogin != null) {
            errors.rejectValue("login", "Duplicate.user.login");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "Required");
        if (user.getPassword().length() < 6 || user.getPassword().length() > 32) {
            errors.rejectValue("password", "Size.user.password");
        }

        if (!user.getConfirmPassword().equals(user.getPassword())) {
            errors.rejectValue("confirmPassword", "Different.user.password");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "Empty.user.email");
        Pattern pattern = Pattern.compile(VALID_EMAIL);
        Matcher matcher = pattern.matcher(user.getEmail());
        if (!matcher.matches()) {
            errors.rejectValue("email", "Valid.user.email");
        }

        if (userFindByEmail != null) {
            errors.rejectValue("email", "Duplicate.user.email");
        }
    }
}