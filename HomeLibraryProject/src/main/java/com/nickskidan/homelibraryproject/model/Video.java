package com.nickskidan.homelibraryproject.model;

public class Video {

    private int id;

    private String videoName;

    private String videoType;

    private byte[] videoData;

    public Video() {
    }

    public Video(int id) {
        this.id = id;
    }

    public Video(String videoName, String videoType, byte[] videoData) {
        this.videoName = videoName;
        this.videoType = videoType;
        this.videoData = videoData;
    }

    public Video(int id, String videoName, String videoType, byte[] videoData) {
        this.id = id;
        this.videoName = videoName;
        this.videoType = videoType;
        this.videoData = videoData;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVideoName() {
        return videoName;
    }

    public void setVideoName(String videoName) {
        this.videoName = videoName;
    }

    public String getVideoType() {
        return videoType;
    }

    public void setVideoType(String videoType) {
        this.videoType = videoType;
    }

    public byte[] getVideoData() {
        return videoData;
    }

    public void setVideoData(byte[] videoData) {
        this.videoData = videoData;
    }
}