package com.nickskidan.homelibraryproject.model;

public class FavoriteTextFile {

    private int userId;

    private int textFileId;

    public FavoriteTextFile(int userId, int textFileId) {
        this.userId = userId;
        this.textFileId = textFileId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getTextFileId() {
        return textFileId;
    }

    public void setTextFileId(int textFileId) {
        this.textFileId = textFileId;
    }
}