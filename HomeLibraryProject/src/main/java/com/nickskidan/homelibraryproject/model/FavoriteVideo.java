package com.nickskidan.homelibraryproject.model;

public class FavoriteVideo {

    private int userId;

    private int videoId;

    public FavoriteVideo(int userId, int videoId) {
        this.userId = userId;
        this.videoId = videoId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getVideoId() {
        return videoId;
    }

    public void setVideoId(int videoId) {
        this.videoId = videoId;
    }
}