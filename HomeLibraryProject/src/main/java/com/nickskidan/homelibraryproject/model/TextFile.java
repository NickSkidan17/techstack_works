package com.nickskidan.homelibraryproject.model;

public class TextFile {

    private int id;

    private String textFileName;

    private String textFileType;

    private byte[] textFileData;

    public TextFile() {
    }

    public TextFile(int id) {
        this.id = id;
    }

    public TextFile(String textFileName, String textFileType, byte[] textFileData) {
        this.textFileName = textFileName;
        this.textFileType = textFileType;
        this.textFileData = textFileData;
    }

    public TextFile(int id, String textFileName, String textFileType, byte[] textFileData) {
        this.id = id;
        this.textFileName = textFileName;
        this.textFileType = textFileType;
        this.textFileData = textFileData;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTextFileName() {
        return textFileName;
    }

    public void setTextFileName(String textFileName) {
        this.textFileName = textFileName;
    }

    public String getTextFileType() {
        return textFileType;
    }

    public void setTextFileType(String textFileType) {
        this.textFileType = textFileType;
    }

    public byte[] getTextFileData() {
        return textFileData;
    }

    public void setTextFileData(byte[] textFileData) {
        this.textFileData = textFileData;
    }
}