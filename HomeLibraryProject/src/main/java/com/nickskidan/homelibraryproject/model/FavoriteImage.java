package com.nickskidan.homelibraryproject.model;

public class FavoriteImage {

    private int userId;

    private int imageId;

    public FavoriteImage(int userId, int imageId) {
        this.userId = userId;
        this.imageId = imageId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }
}