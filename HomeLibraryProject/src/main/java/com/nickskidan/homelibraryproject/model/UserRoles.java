package com.nickskidan.homelibraryproject.model;

public class UserRoles {
    private int user;
    private int role;

    public UserRoles(int user, int role) {
        this.user = user;
        this.role = role;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }
}