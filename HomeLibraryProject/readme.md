**For running the application use task:**

 - _gradle wrapper (has to be executed one time after cloning project)_
 - _docker-compose up_
 
**Initial URL for project is:**
 
 _http://localhost:8088/HomeLibraryProject_